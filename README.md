# Vali IT kutseradar


1. Õpilase tugevad/nõrgad küljed

2. Isiku omadused

3. Nimekiri IT valtkonna professioonidest (kutsetest)

4. Random valiku võimalus

Idee on selles, et lihtsustada IT suuna õpilastel valida IT kutset vastavalt enda isikuomadustele ja tuleviku plaanidele vms.
Selleks tuleb koondada kõik IT valdkonna ametikohad/kutsed ühte keskkonda ning vastavalt isikuomadustele 
ja õpilase tuleviku plaanidele teeb süsteem valiku tema eest ära või vähemalt annab mingisugused suunised ja soovitused.

Must have

Week1

1. Web server