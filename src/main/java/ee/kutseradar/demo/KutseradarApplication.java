package ee.kutseradar.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;


@SpringBootApplication
public class KutseradarApplication implements CommandLineRunner {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(KutseradarApplication.class, args);
    }
     @Override
    public  void run(String... args) throws Exception {
        String sqlKask = "DROP TABLE IF EXISTS kasutajad; " +
                "CREATE TABLE kasutajad (id SERIAL, vastus1 TEXT, vastus2 TEXT, vastus3 TEXT, vastus4 TEXT, vastus5 TEXT, vastus6 TEXT, vastus7 TEXT, vastus8 TEXT, vastus9 TEXT);";
        jdbcTemplate.execute(sqlKask);
        System.out.println("Kasutajad tabel loodud");
    }
}
